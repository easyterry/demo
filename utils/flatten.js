// 初级版
// function flatten (arr, result = []) {
//     for (const iterator of arr) {
//         if(iterator instanceof Array) {
//             flatten(iterator, result)
//         } else {
//             result.push(iterator)
//         }
//     }
//     console.log(result);
//     return result
// }

// flatten([1, 2, [3, 4, [5]]])

// 进阶
// function _flatten (input, depth, strict, output) {
//     output = output || []

//     if(!depth && depth !== 0) {
//         depth = Infinity
//     } else if(depth <= 0) {
//         return output.concat(input)
//     }

//      var idx = output.length
// 		for (var i = 0, length = input.length; i < length; i++) {
// 			var value = input[i]
// 			if (value instanceof Array) {
// 				// Flatten current level of array or arguments object.
// 				if (depth > 1) {
// 					_flatten(value, depth - 1, strict, output)
// 					idx = output.length
// 				} else {
// 					var j = 0,
// 						len = value.length
// 					while (j < len) output[idx++] = value[j++]
// 				}
// 			} else if (!strict) {
// 				output[idx++] = value
// 			}
//         }

//         console.log(output);
// 		return output
// }

// reduce方法
function _flatten(array, depth) {
	if (!depth && depth !== 0) {
		depth = Infinity
	}

	return array.reduce((accumulator, currentValue) => {
		return accumulator.concat(
			depth > 1
				? Array.isArray(currentValue)
					? _flatten(currentValue, depth - 1)
					: currentValue
				: currentValue
		)
	}, [])
}

console.log(_flatten([1, 2, [3, 4, [5, 6, [8, 9]]]], 1))
