var curry = function (fn) {
    var _args = []

    return function cb () {
        if(arguments.length === 0) {
            console.log(fn, _args);
            return fn.apply(this, _args)
        }
        var _innerArgs = Array.prototype.slice.call(arguments)
        _args = _args.concat(_innerArgs)

        console.log(_args);
        return cb
    }
}

var add = function () {
    return Array.prototype.slice.call(arguments).reduce((accumulator, currentValue) => {
        return accumulator + currentValue
    })
}

var sum = curry(add)

console.log(sum(0)(1)(13)());