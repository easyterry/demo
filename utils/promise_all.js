Promise._all = function (promises) {
    return new Promise((resolve, reject) => {
        var resolvedCounter = 0
        var promiseNum = promises.length
        var resolvedList = new Array(promiseNum)

        for (let i = 0; i < promiseNum; i++) {
            Promise.resolve(promises[i]).then(function(val) {
                resolvedCounter++
                resolvedList[i] = val

                if(resolvedCounter === promiseNum) 
                    return resolve(resolvedList)
            }, function (reason) {
                return reject(reason)
            })
        }
    })
}

Promise._race = function (promises) {
    return new Promise((resolve, reject) => {
        var promiseNum = promises.length

        for (let i = 0; i < promiseNum; i++) {
            Promise.resolve(promises[i]).then(function (val) {
                return resolve(val)
            }, function (reason) {
                return reject(reason)
            })
        }
    })
}


function promise1 () {
    return new Promise((resolve) => {
        setTimeout(() => {
            resolve(1)
        }, 3000)
    })
}

function promise2 () {
    return new Promise(resolve => {
        setTimeout(() => {
            resolve(2)
        }, 1000)
    })
}

Promise._all([promise1(), promise2()]).then(res => {
    console.log('all result', res);
})

Promise._race([promise1(), promise2()]).then(res => {
    console.log('race result', res);
})