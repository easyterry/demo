Function.prototype._bind = function (context) {
    if(typeof this !== 'function') {
        throw new Error(
			'Function.prototype.bind - what is trying to be bound is not callable'
		)
    }

    var args = Array.prototype.slice.call(arguments, 1)
    var self = this
    var _fn = function () {}

    var bound = function () {
        if(this instanceof _fn) {
            console.log('new调用');
        } else {
            console.log('普通调用');
        }
        var params = Array.prototype.slice.call(arguments)
        // 判断new调用还是普通调用
        // 1、this instanceof _fn
        // 2、this.constructor === _fn
        self.apply(
			this instanceof _fn ? this : context,
			args.concat(params)
		)
    }

    _fn.prototype = this.prototype
    bound.prototype = new _fn()
    return bound
}

let foo = {
    x: 1
}

function person (name, age) {
    console.log(this.x)
    console.log(name)
    console.log(age)
}

let bound = person._bind(foo, 'terry')
// let bar = bound(25)
let bar = new bound(25)