function debounce (fn, wait, immediate) {
    var context, args, result, timeout

    return function () {
        clearTimeout(timeout)

        args = arguments
        context = this

        if (immediate) {
            var callNow = timeout
            timeout = setTimeout(() => {
                timeout = null
            }, wait)
            if(!callNow) result = fn.apply(context, args)
		} else {
            timeout = setTimeout(() => {
                fn.apply(context, args)
            }, wait)
        }

        return result
    }
}