function throttle (fn, wait) {
    var args, context, previous = 0

    return function () {
        context = this
        args = arguments
        var nowTime = +new Date()

        if(nowTime - previous > wait) {
            fn.apply(context, args)
            previous = nowTime
        }
    }
}