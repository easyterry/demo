function objectFactory (fn, ...args) {
    var newObj = Object.create(fn.prototype)
    var ret = fn.apply(newObj, args)
    return ret instanceof Object ? ret : newObj
}

function person (name, age) {
    this.name = name
    this.age = age
}

var newPerson = objectFactory(person, 'terry', 25)

console.log(newPerson);

