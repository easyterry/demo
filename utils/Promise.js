const PENDING = 'pending'
const RESOLVED = 'resolved'
const REJECTED = 'rejected'

var MyPromise = function (fn) {
    var _this = this;
    _this.status = PENDING
    _this.value = undefined

    _this.resolvedCallBacks = []
    _this.rejectedCallBacks = []

    _this.resolve = function (value) {
        if(value instanceof MyPromise) {
            return value.then(_this.resolve, _this.reject)
        }

        setTimeout(() => {
            _this.status = RESOLVED
            _this.value = value
            _this.resolvedCallBacks.forEach(cb => cb())
        })
    }

    _this.reject = function(reason) {
		setTimeout(() => {
			_this.status = REJECTED
			_this.value = reason
			_this.rejectedCallBacks.forEach((cb) => cb())
		})
	}

    try {
        fn(_this.resolve, _this.reject)
    } catch (error) {
        _this.reject(error)
    }
}

MyPromise.prototype.then = function (onResolved, onRejected) {
    var self = this
    var promise2;
    onResolved = typeof onResolved === 'function' ? onResolved : v => v;
    onRejected = typeof onRejected === 'function' ? onRejected : err => { throw err; }

    if(self.status === PENDING) {
        return (promise2 = new MyPromise(function(resolve, reject) {
            
        }))
    }
}