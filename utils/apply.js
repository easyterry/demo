// Function.prototype._call = function (context) {
//     var args = [], result

//     context.fn = this
//     for(let i = 1, length = arguments.length; i < length; i++) {
//         args.push('arguments[' + i + ']')
//     }

//     result = eval('context.fn('+ args +')')

//     delete context.fn
//     return result   
// }

Function.prototype._call = function(context, ...args) {
	const fn = Symbol()
	try {
		context[fn] = this
		context[fn](...args)
	} catch (e) {
		// Turn primitive types into complex ones 1 -> Number, thanks to Mark Meyer for this.
		context = new context.constructor(context)
		context[fn] = this
		context[fn](...args)
	}
}

function person(x, y) {
    this.x = x;
    this.y = y;
    console.log(x, y);
}

var obj = {
    name: 'Terry'
}

var func = person._call(obj, 1, 2, 3)