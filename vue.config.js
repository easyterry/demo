const target = 'http://localhost:3001/'
const webhookUrl = 'https://qyapi.weixin.qq.com/cgi-bin/webhook/send?key=429ac95e-1b17-4a54-aee5-2472e379cee9'

module.exports = {
	devServer: {
		proxy: {
			'/api': {
				target: target, //对应自己的接口
				changeOrigin: true,
				secure: false,
				pathRewrite: {
					'/api': ''
				}
			},
			'/robot': {
				target: webhookUrl,
				changeOrigin: true,
				secure: false,
				pathRewrite: {
					'/robot': ''
				}
			}
		},
	},
}