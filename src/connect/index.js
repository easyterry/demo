const Koa = require('koa')
const app = new Koa()
const bodyParser = require('koa-bodyparser')

app.use(bodyParser())

app.use(require('./user').routes())
app.use(require('./tapd').routes())

app.listen(3001, () => {
	console.log('listening at 3001')
})