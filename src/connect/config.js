const mysql = require('mysql')
const config = require('./default')

const pool = mysql.createPool({
	host: config.database.HOST,
	port: config.database.PORT,
	user: config.database.USER,
	password: config.database.PASSWORD,
	database: config.database.DATABASE
})

let query = (sql, values) => {
	return new Promise((resolve, reject) => {
		pool.getConnection((err, connection) => {
			if (err) {
				reject(err)
			} else {
				connection.query(sql, values, (err, rows) => {
					if (err) {
						reject(err)
					} else {
						resolve(rows)
					}
					connection.release()
				})
			}
		})
	})
}

let findAliveData = () => {
	// let _sql = 'select count(*) from db_ex_tapd.t_tapd_bug_order'
	let _sql = `select id, title, current_owner, status, created from db_ex_tapd.t_tapd_bug_order
		where created > 2020-09-23 
		and custom_field_two="直播产品中心-直播前端组" 
		and (status="unconfirmed" or status="in_progress")
		and module="技术问题"`

	return query(_sql)
}

const getStatusDuration = (id) => {
	id = '(' + id.map(item => "'" + item + "'").join(',') + ')'

	let _sql = `select bug_id, status, duration from t_tapd_order_status_time where (status="unconfirmed" or status="in_progress") and bug_id in ${id}`

	return query(_sql)
}

// let findAllUsersData = () => {
// 	let _sql = `select * from dbdata`
// 	return query(_sql)
// }

// let findUserData = (name) => {
// 	let _sql = `select * from dbdata where name="${name}";`
// 	return query(_sql)
// }

// let insertUserData = (value) => {
// 	let _sql = `insert into dbdata set name=?, age=?`
// 	return query(_sql, value)
// }

// let deleteUserData = (name) => {
// 	let _sql = `delete from dbdata where name="${name}"`
// 	return query(_sql)
// }

// let updateUserData = (value) => {
// 	let _sql = `update dbdata set name=?, age=? where id=?`
// 	return query(_sql, value)
// }

module.exports = {
	findAliveData,
	getStatusDuration,
	// findAllUsersData,
	// findUserData,
	// insertUserData,
	// deleteUserData,
	// updateUserData,
}