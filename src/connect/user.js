/* eslint-disable */

const router = require('koa-router')()
const userModel = require('./config')

router.prefix('/user')

router.post('/getAllUsers', async (ctx) => {
	await userModel.findAllUsersData().then((res) => {
		if (res.length) {
			ctx.body = {
				code: 0,
				data: res,
				msg: 'get users data successful',
			}
		} else {
			ctx.body = {
				code: 1,
				data: [],
				msg: 'data not found',
			}
		}
	})
})

// 查询数据
router.post('/getUser', async (ctx, next) => {
	await userModel
		.findUserData(ctx.request.body.name)
		.then((res) => {
			if (res.length) {
				ctx.body = {
					code: 0,
					data: res,
					msg: '',
				}
			} else {
				ctx.body = {
					code: 1,
					data: [],
					msg: 'data not found',
				}
			}
		})
		.catch((err) => {
			ctx.throw(500)
		})
})

// 插入数据
router.post('/insertUser', async (ctx) => {
	await userModel.findUserData(ctx.request.body.name).then(async (res) => {
		if (res.length) {
			ctx.body = {
				code: 1,
				msg: 'user has existed',
			}
		} else {
			await userModel
				.insertUserData([ctx.request.body.name, ctx.request.body.age])
				.then(() => {
					ctx.body = {
						code: 0,
						msg: 'insert successful',
					}
				})
				.catch((err) => {
					ctx.throw(500, err)
				})
		}
	})
})

router.post('/deleteUser', async (ctx) => {
	await userModel
		.findUserData(ctx.request.body.name)
		.then(async (res) => {
			if (res.length) {
				await userModel
					.deleteUserData(ctx.request.body.name)
					.then((res) => {
						ctx.body = {
							code: 0,
							msg: 'delete successful',
						}
					})
			} else {
				ctx.body = {
					code: 1,
					msg: 'data not found',
				}
			}
		})
		.catch((err) => {
			ctx.throw(500, err)
		})
})

router.post('/updateUserData', async (ctx) => {
	await userModel
		.updateUserData([ctx.request.body.name, ctx.request.body.age, ctx.request.body.id])
		.then((res) => {
			ctx.body = {
				code: 0,
				msg: 'update successful',
			}
		})
	// await userModel
	// 	.findUserData(ctx.request.body.name)
	// 	.then(async (res) => {
	// 		if(res.length) {
	// 			await userModel
	// 				.updateUserData([
	// 					ctx.request.body.name,
	// 					ctx.request.body.age,
	// 				])
	// 				.then((res) => {
	// 					ctx.body = {
	// 						code: 0,
	// 						msg: 'update successful',
	// 					}
	// 				})
	// 		} else {
	// 			ctx.body = {
	// 				code: 1,
	// 				msg: 'data not found'
	// 			}
	// 		}
	// })
})

module.exports = router
