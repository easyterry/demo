import Vue from 'vue'
import App from './App.vue'
import axios from 'axios'
import { Table, TableColumn, Button, Message, Input, Loading } from 'element-ui'
import 'element-ui/lib/theme-chalk/index.css';

Vue.config.productionTip = false
Vue.prototype.$http = axios
Vue.prototype.$message = Message
Vue.prototype.$loading = Loading.service

Vue.use(Loading.directive)
Vue.use(Table)
Vue.use(TableColumn)
Vue.use(Button)
Vue.use(Input)

new Vue({
  render: h => h(App),
}).$mount('#app')
